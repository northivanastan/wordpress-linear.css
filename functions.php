<?php
if ( ! function_exists( 'linear_setup' ) ) :
function linear_setup() {
register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
     )
   );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );}
endif;
add_action( 'after_setup_theme', 'linear_setup' );
add_theme_support( 'title-tag' );
?>
