<html>
<head>
<?php wp_head();?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Comfortaa|Noto+Sans" rel="stylesheet">
<meta charset="UTF-8">
</head>
<body class=linear>
<div class="segment-head flair-title"><?php bloginfo( 'name' ); ?></div>
<div class="segment-nav flair-nav"><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) );?></div>
<?php if ( has_post_thumbnail()): ?>
<div class="segment-image flair-black" style="background-image:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');"><?php the_title('<h1>','</h1>'); ?></div> <?php endif; ?>